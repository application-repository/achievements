package de.kybu.achievements.example;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import de.kybu.achievements.example.listener.AchievementUnlockListener;
import de.kybu.achievements.example.listener.PlayerDeathListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.concurrent.Executors;

public class AchievementExample extends JavaPlugin {

    public static final ListeningExecutorService LISTENING_EXECUTOR_SERVICE = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()));

    @Override
    public void onLoad() {
    }

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(new AchievementUnlockListener(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerDeathListener(), this);
    }

    @Override
    public void onDisable() {
    }
}

package de.kybu.achievements.example.listener;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import de.kybu.achievements.common.IAchievementPlayerProvider;
import de.kybu.achievements.common.model.IAchievementPlayer;
import de.kybu.achievements.example.AchievementExample;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.checkerframework.checker.nullness.qual.Nullable;

public class PlayerDeathListener implements Listener {

    @EventHandler
    public void handle(final PlayerDeathEvent event){
        IAchievementPlayerProvider playerProvider = IAchievementPlayerProvider.getInstance();
        playerProvider.createPlayerProfile(event.getEntity().getUniqueId());

        Futures.addCallback(playerProvider.getAchievementPlayer(event.getEntity().getUniqueId()), new FutureCallback<IAchievementPlayer>() {
            @Override
            public void onSuccess(@Nullable IAchievementPlayer result) {
                result.addAchievement(2 /* Achievement: Tod lol */);
                playerProvider.updatePlayerProfileSync(result.getUuid(), result);
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        }, AchievementExample.LISTENING_EXECUTOR_SERVICE);
    }

}

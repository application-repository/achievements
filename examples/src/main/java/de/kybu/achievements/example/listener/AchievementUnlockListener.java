package de.kybu.achievements.example.listener;

import de.kybu.achievements.common.event.AchievementUnlockedEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class AchievementUnlockListener implements Listener {

    @EventHandler
    public void handle(final AchievementUnlockedEvent event){
        Player player = Bukkit.getPlayer(event.getUuid());
        if(player == null)
            return;

        player.sendMessage("unlocked achievement " + event.getAchievement().getAchievementName());
    }

}

package de.kybu.achievements.common;

import de.kybu.achievements.common.model.IAchievement;

import java.util.HashMap;
import java.util.Map;

public class IAchievementCache {

    private final Map<Integer, IAchievement> achievements;

    private static IAchievementCache instance;

    public IAchievementCache(){
        this.achievements = new HashMap<>();
    }

    public IAchievement getAchievement(final int achievementId){
        return this.achievements.get(achievementId);
    }

    public void cacheAchievement(final IAchievement achievement){
        achievements.put(achievement.getAchievementId(), achievement);
    }

    public void resetCache(){
        this.achievements.clear();
    }

    public Map<Integer, IAchievement> getAchievements() {
        return achievements;
    }

    public static IAchievementCache getInstance() {
        return (instance != null ? instance : (instance = new IAchievementCache()));
    }
}

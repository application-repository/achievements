package de.kybu.achievements.common.model;

import java.util.List;
import java.util.UUID;

public abstract class IAchievementPlayer {

    private final UUID uuid;
    private List<Integer> unlockedAchievements;

    public IAchievementPlayer(final UUID uuid, List<Integer> unlockedAchievements){
        this.uuid = uuid;
        this.unlockedAchievements = unlockedAchievements;
    }

    public abstract void unlockAchievement(final int achievement);

    public abstract boolean hasAchievement(final int achievement);

    public List<Integer> getUnlockedAchievements() {
        return unlockedAchievements;
    }

    public void addAchievement(final int id){
        this.unlockedAchievements.add(id);
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUnlockedAchievements(List<Integer> unlockedAchievements) {
        this.unlockedAchievements = unlockedAchievements;
    }
}

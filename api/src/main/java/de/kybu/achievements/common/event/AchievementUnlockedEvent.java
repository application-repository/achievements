package de.kybu.achievements.common.event;

import de.kybu.achievements.common.model.IAchievement;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.UUID;

public class AchievementUnlockedEvent extends Event {

    public static HandlerList HANDLERS = new HandlerList();

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    private final UUID uuid;
    private final IAchievement achievement;

    public AchievementUnlockedEvent(final UUID uuid, final IAchievement achievement){
        this.uuid = uuid;
        this.achievement = achievement;
    }

    public UUID getUuid() {
        return uuid;
    }

    public IAchievement getAchievement() {
        return achievement;
    }
}

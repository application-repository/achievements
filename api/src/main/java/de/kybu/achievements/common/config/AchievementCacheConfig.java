package de.kybu.achievements.common.config;

import de.kybu.achievements.common.model.IAchievement;

import java.util.HashMap;
import java.util.Map;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 12.02.2021
 */
public class AchievementCacheConfig {

    private Map<Integer, IAchievement> achievementMap;

    public AchievementCacheConfig(){
        this.achievementMap = new HashMap<>();
    }

    public void add(final IAchievement achievement){
        this.achievementMap.put(achievement.getAchievementId(), achievement);
    }

    public IAchievement get(final int id){
        return this.achievementMap.get(id);
    }

    public Map<Integer, IAchievement> getAchievementMap() {
        return achievementMap;
    }
}

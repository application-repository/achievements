package de.kybu.achievements.common.model;

public class IAchievement {

    private final int achievementId;
    private final String achievementName;
    private final String[] achievementDescription;
    private final String achievementModule;
    private final AchievementDifficulty achievementDifficulty;

    public IAchievement(final int achievementId, final String achievementName, final AchievementDifficulty achievementDifficulty, final String achievementModule, final String... achiementDescription){
        this.achievementId = achievementId;
        this.achievementName = achievementName;
        this.achievementDifficulty = achievementDifficulty;
        this.achievementModule = achievementModule;
        this.achievementDescription = achiementDescription;
    }

    public int getAchievementId() {
        return achievementId;
    }

    public AchievementDifficulty getAchievementDifficulty() {
        return achievementDifficulty;
    }

    public String getAchievementName() {
        return achievementName;
    }

    public String[] getAchievementDescription() {
        return achievementDescription;
    }

    public String getAchievementModule() {
        return achievementModule;
    }
}

package de.kybu.achievements.common;

import com.google.common.util.concurrent.ListenableFuture;
import de.kybu.achievements.common.model.IAchievement;
import de.kybu.achievements.common.model.IAchievementPlayer;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public abstract class IAchievementPlayerProvider {

    private static IAchievementPlayerProvider instance;

    protected IAchievementPlayerProvider(){}

    public static IAchievementPlayerProvider getInstance() {
        return instance;
    }

    protected static void setInstance(IAchievementPlayerProvider achievementPlayerProvider){
        instance = achievementPlayerProvider;
    }

    public abstract ListenableFuture<IAchievementPlayer> getAchievementPlayer(final UUID uuid);

    public abstract IAchievementPlayer getAchievementPlayerSync(final UUID uuid);

    public abstract CompletableFuture<Void> createPlayerProfile(final UUID uuid);

    public abstract void createPlayerProfileSync(final UUID uuid);

    public abstract CompletableFuture<Void> updatePlayerProfile(final UUID uuid, final IAchievementPlayer achievementPlayer);

    public abstract void updatePlayerProfileSync(final UUID uuid, final IAchievementPlayer achievementPlayer);

    public abstract ListenableFuture<List<IAchievement>> getPlayerAchievements(final UUID uuid, final String module);
}

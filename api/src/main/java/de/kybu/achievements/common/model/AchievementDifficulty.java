package de.kybu.achievements.common.model;

public enum AchievementDifficulty {

    EASY,
    NORMAL,
    HARD;

}

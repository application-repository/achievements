package de.kybu.achievements;

import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import de.kybu.achievements.common.IAchievementPlayerProvider;
import de.kybu.achievements.helper.AchievementCacheHelper;
import de.kybu.achievements.user.AchievementPlayerProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.concurrent.Executors;

public class AchievementModule extends JavaPlugin {

    private static AchievementModule instance;
    private final ListeningExecutorService listeningExecutorService;

    public AchievementModule(){
        this.listeningExecutorService = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()));
    }

    @Override
    public void onLoad() {
        instance = this;
    }

    @Override
    public void onEnable() {
        IAchievementPlayerProvider achievementPlayerProvider = new AchievementPlayerProvider();
        try {
            AchievementCacheHelper.cacheAchievements();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    public ListeningExecutorService getListeningExecutorService() {
        return listeningExecutorService;
    }

    public static AchievementModule getInstance() {
        return instance;
    }
}

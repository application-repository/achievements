package de.kybu.achievements.user;

import com.google.common.util.concurrent.ListenableFuture;
import de.kybu.achievements.AchievementModule;
import de.kybu.achievements.common.IAchievementCache;
import de.kybu.achievements.common.IAchievementPlayerProvider;
import de.kybu.achievements.common.model.IAchievement;
import de.kybu.achievements.common.model.IAchievementPlayer;
import de.kybu.achievements.internal.ConfigurationHelper;

import java.io.File;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class AchievementPlayerProvider extends IAchievementPlayerProvider {

    private Map<String, List<Integer>> achievementConfiguration;

    public AchievementPlayerProvider(){
        IAchievementPlayerProvider.setInstance(this);
        this.achievementConfiguration = loadConfiguration();
    }

    @Override
    public ListenableFuture<IAchievementPlayer> getAchievementPlayer(UUID uuid) {
        return AchievementModule.getInstance().getListeningExecutorService().submit(() -> {
            if(!achievementConfiguration.containsKey(uuid.toString()))
                createPlayerProfileSync(uuid);

            List<Integer> iAchievements = this.achievementConfiguration.get(uuid.toString());
            return new AchievementPlayer(uuid, iAchievements);
        });
    }

    @Override
    public IAchievementPlayer getAchievementPlayerSync(UUID uuid) {
        if(!achievementConfiguration.containsKey(uuid.toString()))
            createPlayerProfileSync(uuid);

        List<Integer> iAchievements = this.achievementConfiguration.get(uuid.toString());
        return new AchievementPlayer(uuid, iAchievements);
    }

    @Override
    public void createPlayerProfileSync(UUID uuid) {
        if(!achievementConfiguration.containsKey(uuid.toString())){
            this.achievementConfiguration.put(uuid.toString(), new ArrayList<>());
            ConfigurationHelper.getInstance().saveConfig("plugins/GameFrame/playerAchievements.json", this.achievementConfiguration);
        }
    }

    @Override
    public CompletableFuture<Void> createPlayerProfile(UUID uuid) {
        return CompletableFuture.runAsync(() -> {
            if(!achievementConfiguration.containsKey(uuid.toString())){
                this.achievementConfiguration.put(uuid.toString(), new ArrayList<>());
                ConfigurationHelper.getInstance().saveConfig("plugins/GameFrame/playerAchievements.json", this.achievementConfiguration);
            }
        });
    }

    @Override
    public CompletableFuture<Void> updatePlayerProfile(UUID uuid, IAchievementPlayer achievementPlayer) {
        return CompletableFuture.runAsync(() -> updatePlayerProfileSync(uuid, achievementPlayer));
    }

    @Override
    public void updatePlayerProfileSync(UUID uuid, IAchievementPlayer achievementPlayer) {
        this.achievementConfiguration.remove(uuid.toString());
        this.achievementConfiguration.put(uuid.toString(), achievementPlayer.getUnlockedAchievements());

        ConfigurationHelper.getInstance().saveConfig("plugins/GameFrame/playerAchievements.json", this.achievementConfiguration);
    }

    @Override
    public ListenableFuture<List<IAchievement>> getPlayerAchievements(UUID uuid, String module) {
        return AchievementModule.getInstance().getListeningExecutorService().submit(() -> {
            return getAchievementPlayer(uuid).get().getUnlockedAchievements().stream()
                    .map(integer -> IAchievementCache.getInstance().getAchievement(integer))
                    .filter(achievement -> achievement.getAchievementModule().equalsIgnoreCase(module))
                    .collect(Collectors.toList());
        });
    }

    private ConcurrentHashMap<String, List<Integer>> loadConfiguration(){
        final String tempAchievementConfigPath = "plugins/GameFrame/playerAchievements.json";
        final File achievementConfigFile = new File(tempAchievementConfigPath);
        if(!achievementConfigFile.exists()){
            this.achievementConfiguration = new ConcurrentHashMap<>();
            ConfigurationHelper.getInstance().saveConfig(tempAchievementConfigPath, this.achievementConfiguration);
        }

        return ConfigurationHelper.getInstance().loadConfig(tempAchievementConfigPath, (Class<ConcurrentHashMap<String, List<Integer>>>) (Class) ConcurrentHashMap.class);
    }
}

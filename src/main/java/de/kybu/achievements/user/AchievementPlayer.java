package de.kybu.achievements.user;

import de.kybu.achievements.common.IAchievementCache;
import de.kybu.achievements.common.IAchievementPlayerProvider;
import de.kybu.achievements.common.event.AchievementUnlockedEvent;
import de.kybu.achievements.common.model.IAchievement;
import de.kybu.achievements.common.model.IAchievementPlayer;
import org.bukkit.Bukkit;

import java.util.List;
import java.util.UUID;

public class AchievementPlayer extends IAchievementPlayer {

    public AchievementPlayer(UUID uuid, List<Integer> unlockedAchievements) {
        super(uuid, unlockedAchievements);
    }

    @Override
    public void unlockAchievement(int achievement) {
        addAchievement(achievement);
        IAchievement iAchievement = IAchievementCache.getInstance().getAchievement(achievement);
        IAchievementPlayerProvider.getInstance().updatePlayerProfileSync(getUuid(), this);
        Bukkit.getPluginManager().callEvent(new AchievementUnlockedEvent(getUuid(), iAchievement));
    }

    @Override
    public boolean hasAchievement(int achievement) {
        for (Object object : this.getUnlockedAchievements()) {
            int id;
            if(object instanceof Double)
                id = ((Double) object).intValue();
            else
                id = (int) object;

            if(id == achievement)
                return true;
        }
        return false;
    }
}

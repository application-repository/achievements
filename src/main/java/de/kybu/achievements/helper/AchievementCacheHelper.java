package de.kybu.achievements.helper;

import de.kybu.achievements.common.IAchievementCache;
import de.kybu.achievements.common.config.AchievementCacheConfig;
import de.kybu.achievements.common.model.AchievementDifficulty;
import de.kybu.achievements.common.model.IAchievement;
import de.kybu.achievements.internal.ConfigurationHelper;

import java.io.File;
import java.io.IOException;

public class AchievementCacheHelper {

    public static void cacheAchievements() throws IOException {
        IAchievementCache.getInstance().resetCache();

        File achievementsFile = new File("plugins/GameFrame/modules/achievements.json");
        if(!achievementsFile.exists()){
            AchievementCacheConfig achievementCacheConfig = new AchievementCacheConfig();
            achievementCacheConfig.add(new IAchievement(0, "Module", AchievementDifficulty.HARD, "Module", "Line1", "Line2"));
            ConfigurationHelper.getInstance().saveConfig(achievementsFile.getPath(), achievementCacheConfig);
        }

        AchievementCacheConfig achievementCacheConfig = ConfigurationHelper.getInstance().loadConfig(achievementsFile.getPath(), AchievementCacheConfig.class);
        achievementCacheConfig.getAchievementMap().values().forEach(iAchievement -> {
            IAchievementCache.getInstance().cacheAchievement(iAchievement);
        });

    }

}

package de.kybu.achievements.internal;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Class Description following...
 *
 * @author Felix Clay (kybuu)
 * @since 12.02.2021
 */
public class ConfigurationHelper {

    private static ConfigurationHelper instance;

    private final Gson gson;

    public static ConfigurationHelper getInstance() {
        return (instance != null ? instance : (instance = new ConfigurationHelper()));
    }

    public ConfigurationHelper(){
        this.gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    public <T> T loadConfig(final String path, final Class<T> clazz){
        try {
            return gson.fromJson(String.join("", Files.readAllLines(Paths.get(path))), clazz);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public <T> void saveConfig(final String path, final T t){
        try(FileWriter fileWriter = new FileWriter(path)){
            gson.toJson(t, fileWriter);
        } catch(IOException ex){
            ex.printStackTrace();
        }
    }
}
